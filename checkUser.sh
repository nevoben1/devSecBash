#! /usr/bin/env bash 
########################################
# created by : nevo
# purpose : conditionals
# version : 0.0.1
#set -e
#set -u
#set -x
##########################################
if [[ "$#" == "0" ]];then
	echo "you must give at least one argument"
	exit 1
fi

while (( $# ))
do
	echo "you gave me the argument $1"
shift
done

