#! /usr/bin/env bash
###############################################
# Created by : Nevo
# Purpose: loops
# Version : 0.0.1
# Date : 27.12.2021
#set -e
#set -x
###############################################

i=$1

while [[ $i -lt 100 ]]
do
	echo $i
	if ((i%7==0));then
		echo BOOM
		let i++
		continue
		
	elif ((i%3==0));then
		echo Gotcha
		break
	fi
	sleep 0.2
	let i++
done	




	

