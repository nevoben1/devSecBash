#! /usr/bin/env bash
###############################################
# Created by : Nevo
# Purpose: loops
# Version : 0.0.1
# Date : 27.12.2021
#set -e
#set -x
############################################

while getopts ":afz" option;
do
	case $option in
		a)
			echo recived -a
			;;
		f)
			echo recived -f
			;;
		z)
			echo recived -z
			;;
		*)
			echo invalid option -$OPTARG
			;;
	esac
done

