#! /usr/bin/env bash 
#########################################
# created by : Nevo
# purpose : operators
# version : 0.0.1
# date : 27.12.2021
set -x
#set -e
###########################################

# Logical operators
# and --> &&
# or --> ||
# not --> !

echo 0 && echo 0 ; echo $?

echo 1 || eho 0 ; echo $?
! echo 1 ; echo $?

# Existance operators
# exists  -e
# exists and is regular   -f
# exists and is directory   -d
# exists and is runable   -x
# exists and is readable  -r
# exists and is writeable -w
# no value -z
# has some value -n
# exists and is symbolic link -L


echo '-e' && [[ -e /etc/passwd ]] ; echo $?
echo '-f' && [[ -f /etc/passwd ]] ; echo $?
echo '-r' && [[ -r /etc/passwd ]] ; echo $?
[[ -w $0 ]] ; echo $?
[[ -d /etc/passwd ]] ; echo $?
[[ -x $0 ]] ; echo $?
[[ -L /etc/passwd ]] ; echo $?

echo '-z' && [[ -z $SHELL ]] ; echo $?
echo '-n' && [[ -n $SHELL ]] ; echo $?
