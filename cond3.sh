#! /usr/bin/env bash
###########################################
# Created by : Nevo
# Purpose : conditional structs
# Version : 0.0.1
# Date : 27.12.21
#set -e
set -x
##########################################

var=$1

if [[ -n $var ]];then
	curl -v $var | grep 'HTTP/1.1'
else 
	echo " why so $var is so low"
fi
