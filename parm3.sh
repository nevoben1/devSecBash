#! /usr/bin/env bash
##############################
# created by : Nevo
# purpose : to work with positional params
# version : 0.0.0
##############################

var=${10}
echo "$0 has gotten the variable ${10}"
