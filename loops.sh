#! /usr/bin/env bash
###############################################
# Created by : Nevo
# Purpose: loops
# Version : 0.0.1
# Date : 27.12.2021
#set -e
#set -x
###############################################

for index in 1 2 3 4 5 6 7
do
	echo $index
done
