#! /usr/bin/env bash
###############################################
# Created by : Nevo
# Purpose: loops
# Version : 0.0.1
# Date : 27.12.2021
#set -e
#set -x
###############################################

index=0
while [[ $index -lt 100 ]]
do
	echo $index
	let index++
	
done
