#! /usr/bin/env bash 
########################################
# created by : nevo
# purpose : conditionals
# version : 0.0.1
#set -e
set -u
set -x
##########################################

num1=5
num2=4
 [ $num1 -gt $num2 ]; echo $? # greater then
 [ $num1 -lt $num2 ]; echo $? # less then
 [ $num1 -ge $num2 ]; echo $? # graeter or equal then
 [ $num1 -le $num2 ]; echo $? # less or equal then
 [ $num1 -ne $num2 ]; echo $? # not equal
 [ $num1 -eq $num2 ]; echo $? # is equal

