#! /usr/bin/env bash
##############################
# created by : Nevo
# purpose : to work with positional params
# version : 0.0.0
##############################
shift 3 
name=$1
lname=$2
age=$3
echo "Hello : my name is $name $lname and i am $age"
