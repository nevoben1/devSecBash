#! /usr/bin/env bash
###########################################
# Created by : Nevo
# Purpose : conditional structs
# Version : 0.0.1
# Date : 27.12.21
#set -e
set -x
##########################################

var=$1

if [[ $var -ge 42 ]] || [[ $var -gt 45 ]] ;then
	echo "the answer is $var"
fi
