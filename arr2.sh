#! /usr/bin/env bash
##############################
# created by : Nevo
# purpose : to work with positional arrays
# version : 0.0.0
##############################

name=$1
lname=$2
age=$3

multi=($name $lname $age)
echo "Hello : my name is ${multi[0]} ${multi[1]} and i am ${multi[2]}"
