#! /usr/bin/env bash
###############################################
# Created by : Nevo
# Purpose: loops
# Version : 0.0.1
# Date : 27.12.2021
#set -e
#set -x
###############################################
# counting from 1 to 20
for i in {1..20}
do
	echo counting from 1 to 20 , now at $i
	sleep 1
done
